#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

signals:
    void sendCounter(int);

public slots:
    void addCounter();

private:
    Ui::Widget *ui;
    QTimer *timer;
    int counter = 0;
};
#endif // WIDGET_H
