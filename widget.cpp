#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    timer = new QTimer(this);
    timer->start(250);
    connect(timer, SIGNAL(timeout()), this, SLOT(addCounter()));
    connect(this, SIGNAL(sendCounter(int)), ui->lcdCounter, SLOT(display(int)));
}


void Widget::addCounter(){
    this->counter++;
    emit this->sendCounter(this->counter);
}


Widget::~Widget()
{
    delete ui;
}

